from flask import Flask, request, jsonify
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://trung:trung@localhost/movie'
app.config['SQLALCHEMY_TRACKING_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
CORS(app)

class Movie(db.Model):
    id = db.Column(db.Integer,primary_key =True)
    name = db.Column(db.String(123),unique = True)
    year = db.Column(db.Integer)

    def __init__(self,name,year):
        self.name = name 
        self.year =year

class MovieSchema(ma.Schema) :
    class Meta : 
        fields = ('id', 'name', 'year')

movie_schema = MovieSchema()
movies_schema = MovieSchema(many = True)



# create a Movie
@app.route('/movie', methods =['POST'])
def add_movie():
    name = request.json['name']
    year = request.json['year']

    new_movie = Movie(name,year)

    db.session.add(new_movie)
    db.session.commit()

    return movie_schema.jsonify(new_movie)


# select all movies
@app.route('/showallmovie', methods = ['GET'])
def get_movies():
    all_movie = Movie.query.all()
    result = movies_schema.jsonify(all_movie)
    return result


# get all similar data 
@app.route('/movie/year=<yearSubmit>',methods = ['GET'])
def get_movie_on_demand(yearSubmit):
    movies = Movie.query.filter_by(year = yearSubmit).all()
    result = movies_schema.jsonify(movies)
    return result

        
# select single data
@app.route('/movie/id=<id>', methods = ['GET'])
def get_movie(id):
    movie = Movie.query.get(id)
    # "get" methods only get data through primary key in the table
    result = movie_schema.jsonify(movie)
    return result

    
# @app.route('/', methods=['GET','POST'])
# def index():
#     value = request.json['key']
#     return value
    
if __name__ == '__main__':
    app.run(debug=True)