import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesyearPage } from './moviesyear.page';

describe('MoviesyearPage', () => {
  let component: MoviesyearPage;
  let fixture: ComponentFixture<MoviesyearPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviesyearPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesyearPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
