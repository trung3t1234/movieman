import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllmoviesPage } from './allmovies.page';

describe('AllmoviesPage', () => {
  let component: AllmoviesPage;
  let fixture: ComponentFixture<AllmoviesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllmoviesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllmoviesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
