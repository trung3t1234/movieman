import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { Movie } from 'src/app/models/movie'

@Component({
  selector: 'app-allmovies',
  templateUrl: './allmovies.page.html',
  styleUrls: ['./allmovies.page.scss'],
})
export class AllmoviesPage implements OnInit {
  movie : Movie = { 
    id: 1,
    name: "Star wars",
    year: 1977
  }
  data: any;
  error: string;

  constructor(private http: HttpClient) {
    this.data = '';
    this.error = '';
  }

  ngOnInit(){ 

  }

  ionViewWillEnter() {
    // Load the data
    this.prepareDataRequest()
      .subscribe(
        (data: any) => {
          // Set the data to display in the template
          this.data =  data;
        },
        err => {
          // Set the error information to display in the template
          this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
        }
      );

  }

  private prepareDataRequest(): Observable<object> {
    // Define the data URL
    const dataUrl = 'http://127.0.0.1:5000/showallmovie';
    // Prepare the request
    return this.http.get(dataUrl);
  }
}
