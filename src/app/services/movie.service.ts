import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) { }

  // showAllMovie(): Observable<any> {
  //   return this.http.get(`http://127.0.0.1:5000/showallmovie`).pipe(
  //     map(results => results['Show'])
  //   );
  // }

  showAllMovie = () => {
    return this.http.get<any>(`https://google.com`, { observe: 'response' }).subscribe(
      (res: any) => console.log(res),
      err => console.log(err)
      )
  }

}
