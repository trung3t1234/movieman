import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  { path: 'allmovies', loadChildren: './pages/allmovies/allmovies.module#AllmoviesPageModule' },
  { path: 'moviesyear', loadChildren: './pages/moviesyear/moviesyear.module#MoviesyearPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }